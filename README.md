# workstation
Configures a developer workstation.

# WSL 2
* Install [Windows 10 Version 2004](https://en.wikipedia.org/wiki/Windows_10_version_history#Version_2004_(May_2020_Update)) or later.
* Run the following commands from PowerShell to enable the Windows Subsystem for Linux feature:
  ```
  Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
  Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
  ```
* Update the [WSL 2 Linuxl kernel](https://docs.microsoft.com/en-us/windows/wsl/wsl2-kernel).
* From PowerShell run the following to set WSL version 2 as the default version:
  ```
  wsl --set-default-version 2
  ```
* Install the [Ubuntu 20.04](https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71) app.
* Launch the app and during Ubuntu setup, enter `ec2-user` as the username.
* From the Ubuntu terminal run the following to bootstrap the workstation:
  ```
  curl https://gitlab.com/aw5academy/chef/workstation/raw/master/setup.sh | bash
  ```
* From PowerShell run the following to set Ubuntu-20.04 as the default WSL distribution:
  ```
  wsl -s Ubuntu-20.04
  ```

# Extras
## Docker Desktop
* Install [Docker Desktop](https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe).
* Open Docker Desktop settings and under Resources > WSL Integration ensure 'Ubuntu-20.04' integration is enabled.

## Visual Studio Code
* Install [Visual Studio Code](https://code.visualstudio.com/Download) by downloading and running the System Installer.
* Launch Visual Studio Code and enable the Remote - WSL (ms-vscode-remote.remote-wsl) extension.
* From the Ubuntu terminal you can run the following to launch Visual Studio code:
  ```
  code .
  ```

## PuTTY
* You can access the workstation via SSH using PuTTY.
* As a one time operation, open `C:\workstation` in File Explorer and run the `workstation.reg` to load PuTTY settings.
* Open the `workstation.lnk` shortcut link to login to your workstation from PuTTY.
* Note: on every boot you will need to open the Ubuntu terminal once in order to start the SSH service on Ubuntu before you can use PuTTY.
* If you have issues connecting try running the following from PowerShell:
  ```
  wsl --shutdown
  ```

## AWS CodeBuild Local
* The workstation can be configured with [local build support for AWS CodeBuild](https://aws.amazon.com/blogs/devops/announcing-local-build-support-for-aws-codebuild/).
* To configure it, from the Ubuntu terminal, run the following (note: this operation can take up to 60 minutes to complete):
  ```
  bash -x ~/codebuild_setup.sh
  ```
* When configured, you can run the following from any directory containing a valid buildspec.yaml to run a local CodeBuild project:
  ```
  awscb
  ```

# Notes
## Convert WSL to WSL 2
* If you already have a WSL distribution installed you can convert it to WSL 2 by running the following from PowerShell:
  ```
  wsl --set-version Ubuntu-20.04 2
  ```
