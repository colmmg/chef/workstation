#!/bin/bash

startSshAgent() {
  if [ ! "`pgrep ssh-agent`" ]; then
    ssh-agent -s > ~/.ssh/.agent.pid
  fi
  source ~/.ssh/.agent.pid > /dev/null
}

addKeysForSshAgent() {
  if [ -d ~/.ssh/aws-keys ]; then
    for ec2key in `ls -1 ~/.ssh/aws-keys`; do
      ssh-add -k ~/.ssh/aws-keys/$ec2key 2> /dev/null
    done
  fi
}

startSshAgent
addKeysForSshAgent
