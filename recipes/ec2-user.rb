group 'ec2-user' do
  append true
  system false
  action :create
end

user 'ec2-user' do
  gid 'ec2-user'
  comment 'EC2 Default User'
  home '/home/ec2-user'
  shell '/bin/bash'
  action :create
end

execute 'set no expiry on ec2-user password and account' do
  command 'chage -m 0 -M 99999 -I -1 -E -1 ec2-user'
end

directory "/home/ec2-user" do
  owner "ec2-user"
  group "ec2-user"
  mode '0755'
  action :create
end

cookbook_file '/home/ec2-user/.bashrc' do
  source 'home/ec2-user/bashrc'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0644'
  action :create
end

cookbook_file '/home/ec2-user/.bash_profile' do
  source 'home/ec2-user/bash_profile'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0644'
  action :create
end

cookbook_file '/home/ec2-user/.vimrc' do
  source 'home/ec2-user/vimrc'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0644'
  action :create
end

cookbook_file '/home/ec2-user/.inputrc' do
  source 'home/ec2-user/inputrc'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0644'
  action :create
end

directory '/home/ec2-user/.ssh' do
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end

cookbook_file '/home/ec2-user/.ssh/config' do
  source 'home/ec2-user/ssh/config'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0600'
  action :create
end

cookbook_file '/home/ec2-user/.ssh/.agent.sh' do
  source 'home/ec2-user/ssh/agent.sh'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end

directory '/home/ec2-user/.terraform.d/plugin-cache' do
  recursive true
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end

cookbook_file '/etc/sudoers.d/ec2-user' do
  source 'etc/sudoers.d/ec2-user'
  owner 'root'
  group 'root'
  mode '0440'
  action :create
end

directory '/home/ec2-user/.aws' do
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end

cookbook_file '/home/ec2-user/.aws/credentials' do
  source 'home/ec2-user/aws/credentials'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0755'
  action :create
end
