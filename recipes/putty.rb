package "putty-tools" do
  action :install
end

package "net-tools" do
  action :install
end

directory "/mnt/c/workstation" do
  action :create
  recursive true
end

execute 'puttygen' do
  command "puttygen /home/ec2-user/.ssh/id_rsa -o /mnt/c/workstation/workstation.ppk"
  not_if { ::File.exist?('/mnt/c/workstation/workstation.ppk') }
end

cookbook_file '/mnt/c/workstation/workstation.lnk' do
  source 'mnt/c/workstation/workstation.lnk'
  action :create
end

cookbook_file '/mnt/c/workstation/workstation.reg' do
  source 'mnt/c/workstation/workstation.reg'
  action :create
end
