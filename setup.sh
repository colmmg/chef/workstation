#!/bin/bash

CHEF_URL="https://packages.chef.io/files/stable/chef-workstation/20.6.62/ubuntu/20.04/chef-workstation_20.6.62-1_amd64.deb"

downloadAndInstallChef() {
  if which chef > /dev/null; then
    return 0
  fi
  wget "$CHEF_URL" -O /tmp/chef.deb
  sudo dpkg -i /tmp/chef.deb
  rm -f /tmp/chef.deb
}

berksPackage() {
  local tempdir=`mktemp -d`
  pushd $tempdir > /dev/null
  git clone https://gitlab.com/colmmg/chef/workstation.git .
  berks package /tmp/cookbooks.tar.gz
  popd > /dev/null
  rm -rf $tempdir
}

chefConverge() {
  if [ -d /etc/chef/bootstrap ]; then
    sudo rm -rf /etc/chef/bootstrap
  fi
  sudo mkdir -p /etc/chef/bootstrap
  sudo chown ec2-user: /etc/chef /etc/chef/bootstrap
  pushd /etc/chef/bootstrap > /dev/null
  tar zxf /tmp/cookbooks.tar.gz
  rm -f /tmp/cookbooks.tar.gz
  cp -r cookbooks/workstation/environments .
  sudo chef-client --chef-license accept -z -E build -o workstation
  popd > /dev/null
}

downloadAndInstallChef
berksPackage
chefConverge
