describe package('sudo') do
  it { should be_installed }
end

describe package('git') do
  it { should be_installed }
end

describe package('wget') do
  it { should be_installed }
end

describe package('unzip') do
  it { should be_installed }
end

describe package('python3') do
  it { should be_installed }
end

describe package('python3-pip') do
  it { should be_installed }
end

describe package('tree') do
  it { should be_installed }
end

describe package('jq') do
  it { should be_installed }
end

describe package('ruby') do
  it { should be_installed }
end

describe command('pip3 show awscli') do
   its('stdout') { should match (/awscli/) }
end

describe command('pip3 show boto3') do
   its('stdout') { should match (/boto3/) }
end
